# titivillus-LovePeopleUseThings

An **audio transcribing** personal non-commercial personal project for the online podcast [*Love People Use Things*](https://lovepeopleusethings.fm/) run by a Christian (Matt) and an atheist (Noah). They both agree that your life, your relationships, and society as a whole would be a whole lot more beautiful and healthy without porn. 

I do agree too. In case you're interested in the subject, this [review article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4600144/) leads to the conclusion that Internet pornography addiction fits into the addiction framework and shares similar basic mechanisms with substance addiction.

## How to use

#### Requirements

Any setup of your taste is good. In case you don't even have one, I recommend the use of [Atom](https://atom.io/) editor plus [atom-transcribe](https://github.com/mbroedl/atom-transcribe) plugin. It will enable some fancy functionalities to aid the manual speech to text transcription long process, like automatically rewinding the audio source for two seconds in each consecutive play/pause, inserting and navigating trough timestamps, and many more.

On Debian/Ubuntu Linux distributions, the [recommended way](https://flight-manual.atom.io/getting-started/sections/installing-atom/#platform-linux) to install Atom is:

```bash
$ curl -sL https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
$ sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
$ sudo apt update
$ sudo apt install atom
```

Then, install the ```atom-transcribe``` package through the user interface (Edit>Preferences>Install) or the terminal with:

```bash
$ apm install atom-transcribe
```

Furthermore, to have a better transcribing experience, I highly encourage you to [follow this flight manual](https://flight-manual.atom.io/) to learn to fly with and adopt Atom as your text editor. In addition, you should give these other packages a try:

```bash
$ apm install file-icons hard-wrap minimap-autohider minimap-highlight-selected wordcount
```

**Note**: For the ```wordcount``` package to work properly, go to its settings tab (Edit>Preferences>Packages>Wordcount) and activate the "Autoactivate for files without a extension" box.

#### Usage

Clone the project, enter to the main directory and run Atom from a terminal:

```bash
$ cd
$ git clone git@gitlab.com:amartin1911/titivillus-LovePeopleUseThings.git
$ cd titivillus-LovePeopleUseThings/
$ atom .
```

Once inside Atom toggle the ```atom-transcribe``` plugin toolbar (Packages>Transcribe>Toggle) in order to load the audio source file and control its playback. Now you're ready to do some editing! :)

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>. However, for the files under /audio folder, I do now own any rights. These audio files have been streamed and recorded from the internet and are used for educational purposes only.

## Credit

To the authors of Love People Use Things online podcast.
